# Contao-Slick-Slider

## Function & usage

This extension for Contao Open Source CMS allows you to create a slider as content element. It is based on the [slick slider by kenwheeler](https://kenwheeler.github.io/slick/) and adapted for Contao 4.9. Also it is extended with additional TailwindCSS classes and components (`btn-arrow`, `btn-dots`) to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit). 
